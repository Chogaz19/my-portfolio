import React, { useState } from 'react'
import { TabOptions, Title } from '../components'

export default function Whyme() {

    const totalExperienceYears = Number(new Date().getFullYear()) - 2018
    const [whyMeText, setWhyMeText] = useState(`With ${totalExperienceYears} years of experience in web development, I am passionate about technology and dedicated to implementing effective solutions using the best methodologies. My commitment to excellence ensures that each project is crafted with precision and creativity, delivering outstanding results that meet your needs and exceed your expectations. Let's bring your vision to life with innovative and reliable web solutions.`)

    return (
        <div id="why-me" className='w-screen h-auto flex flex-col bg-white justify-center items-center md:px-24 px-6 mt-28'>
            <Title type='title' textPrimary='Why' textSecondary='me?' where='other'/>
            <span className='font-light text-tertiary text-justify md:text-lg text-base mt-14'>{whyMeText}</span>
            <TabOptions/>
        </div>
    )
}

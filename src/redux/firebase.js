// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app"
import { getAnalytics } from "firebase/analytics"
import {addDoc, collection, getFirestore} from "firebase/firestore"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries


// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD7DPfX2OgsyDXXDVtfnMQ_xl9K-RN2orY",
  authDomain: "carlosugazperales-peru.firebaseapp.com",
  databaseURL: "https://carlosugazperales-peru.firebaseio.com",
  projectId: "carlosugazperales-peru",
  storageBucket: "carlosugazperales-peru.appspot.com",
  messagingSenderId: "1016479135808",
  appId: "1:1016479135808:web:108dcda46b4d2c300887a5",
  measurementId: "G-35QRQQWT3D"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
//const analytics = getAnalytics(app);
const firestore = getFirestore(app)
export const crearEmail = (item) => addDoc(collection(firestore,"email"),item)

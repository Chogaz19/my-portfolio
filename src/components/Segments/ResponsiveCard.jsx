import React, {useState } from 'react'
import styles from '../../modules/responsiveCard.module.sass'
import {IconMobile, IconWeb, IconEdit, IconDelete} from '../../components'

export default function ResponsiveCard() {
    const [isResponsive, setIsResponsive] = useState(false)

    const changeView = (view) => {
        setIsResponsive(view === 'web')
    }
    const imageCard = 'https://firebasestorage.googleapis.com/v0/b/carlosugazperales-peru.appspot.com/o/71xePMf3QxL._AC_UF1000%2C1000_QL80_.jpg?alt=media&token=da2dff49-1d96-4382-aa26-5174e4841911'
    return (
        <div className={`${styles.contentCard} ${isResponsive ? styles.responsive : ''}`}>
            <div className={styles.actionsResponsive}>
                <div onClick={()=> changeView('mobile')} className={`${!isResponsive ? styles.active : styles.disabled }`}><IconMobile/></div>
                <div onClick={()=> changeView('web')}    className={`${isResponsive ? styles.active : styles.disabled }`}><IconWeb/></div>
            </div>
            <div className={styles.responsiveCard}>
                <img className={styles.image} src={imageCard}  alt='Fullmetal Alchemist'/>
                <div className={styles.data} >
                    <div className={styles.price}>$1000.00</div>
                    <div className={styles.information}>
                        <div className={styles.description}>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi, possimus?</div>
                        <div className={styles.actions}>
                            <div className={styles.edit}><IconEdit/> </div>
                            <div className={styles.delete}><IconDelete/></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

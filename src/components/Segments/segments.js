const mediaplayerCode = `import React from 'react'
import styles from '../../modules/mediaPlayer.module.sass'
import {IconPause, IconPlayPause, IconPlayPauseBack} from '../../components'

export default function Mediaplayer() {
  return (
    <div className={styles.cardMedia}>
        <div className={styles.cover}>
            <div className={styles.photo}></div>
            <div className={styles.information}>
                <span  className={styles.album}>Asylum</span>
                <span  className={styles.name}>The Infection</span>
                <span  className={styles.author}>Disturbed</span>
            </div>
        </div>
        <div className={styles.timeline}>
            <div className={styles.backgroundTime}>
                <div className={styles.bar}></div>
            </div>
            <div className={styles.time}>
                <span className={styles.start}>2:32</span>
                <span className={styles.end}>4:55</span>
            </div>
        </div>
        <div className={styles.buttons}>
            <button className={styles.secondaryButtons}>
                <IconPlayPauseBack/>
            </button>
            <button className={styles.principalButton}>
                <IconPause/>
            </button>
            <button className={styles.secondaryButtons}>
                <IconPlayPause/>
            </button>
        </div>
    </div>
  )
}`;

const responsiveDesign = `import React, { useState } from 'react'
import styles from '../../modules/responsiveCard.module.sass'
import { IconMobile, IconWeb, IconEdit, IconDelete } from '../../components'

export default function ResponsiveCard() {
    const [isResponsive, setIsResponsive] = useState(false)

    const changeView = (view) => {
        setIsResponsive(view === 'web')
    }
    const imageCard = 'https://firebasestorage.googleapis.com/v0/b/carlosugazperales-peru.appspot.com/o/71xePMf3QxL._AC_UF1000%2C1000_QL80_.jpg?alt=media&token=da2dff49-1d96-4382-aa26-5174e4841911'

    return (
        <div className={\`{styles.contentCard} \{isResponsive ? styles.responsive : ''}\`}>
            <div className={styles.actionsResponsive}>
                <div onClick={() => changeView('mobile')} className={\`\${!isResponsive ? styles.active : styles.disabled}\`}><IconMobile/></div>
                <div onClick={() => changeView('web')} className={\`\${isResponsive ? styles.active : styles.disabled}\`}><IconWeb/></div>
            </div>
            <div className={styles.responsiveCard}>
                <img className={styles.image} src={imageCard} alt='Fullmetal Alchemist book'/>
                <div className={styles.data}>
                    <div className={styles.price}>$1000.00</div>
                    <div className={styles.information}>
                        <div className={styles.description}>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi, possimus?</div>
                        <div className={styles.actions}>
                            <div className={styles.edit}><IconEdit/></div>
                            <div className={styles.delete}><IconDelete/></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}`

const creativeDesign = `import React from 'react'
import styles from '../../modules/creativeDesign.module.sass'

//All magic is in css

export default function CreativeDesign() {
  return (
    <div className={styles.contenedor}>
        <div className={styles.sombra}></div>
        <div className={styles.gatito}>
            <div className={styles.orejita}></div>
            <div className={styles.ojitos}></div>
            <div className={styles.boca}></div>
            <div className={styles.boquita}></div>
            <div className={styles.nariz}></div>
            <div className={styles.colita}></div>
            <div className={styles.cuerpo}></div>
            <div className={styles.moco}></div>
        </div>
    </div>

  )
}

`

const versatileDesign = `/*

There is nothing that cannot be done, we just have to 
write code, google and drink a lot of coffee.

*/

import React from 'react';
import styles from '../../modules/versatileDesign.module.sass';

export default function VersatileDesign() {
  return (
    <div className={styles.containCube}>
        <div className={styles.cube}>
        <div className={styles.top}></div>
        <div>
            <span className={styles.span} style={{ '--i': 0 }}></span>
            <span className={styles.span} style={{ '--i': 1 }}></span>
            <span className={styles.span} style={{ '--i': 2 }}></span>
            <span className={styles.span} style={{ '--i': 3 }}></span>
        </div>
        </div>
    </div>
  );
}


`
const coopAi = `//👨‍💻 Hi, Chat gpt, generete for me please a simple calculator using React.

/*🤖 Hello! Sure, I'll be happy to help you create a basic calculator component 
in React. Here's an example of how you can implement it: */

import React from 'react'
import { useState } from 'react';
import styles from '../../modules/coopAi.module.sass'

export default function CoopAi() {
    const [num1, setNum1] = useState(0);
    const [num2, setNum2] = useState(0);
    const [result, setResult] = useState(0);

    const handleNum1Change = (e) => {
        setNum1(Number(e.target.value));
    };
    const handleNum2Change = (e) => {
        setNum2(Number(e.target.value));
    };
    const handleAddition = () => {
        setResult(num1 + num2);
    };
    const handleSubtraction = () => {
        setResult(num1 - num2);
    };
    const handleMultiplication = () => {
        setResult(num1 * num2);
    };
    const handleDivision = () => {
        setResult(num1 / num2);
    };
    return (
        <div className={styles.cardCalculator}>
            <div className={styles.result}>
                <h2 className={styles.number}>{result}</h2>
            </div>
            <div className={styles.inputsCalculator}>
                <input className={styles.inputCal} type="number" value={num1} onChange={handleNum1Change} />
                <input className={styles.inputCal} type="number" value={num2} onChange={handleNum2Change} />
            </div>
            <div className={styles.buttons}>
                <button className={styles.button} onClick={handleAddition}>+</button>
                <button className={styles.button} onClick={handleSubtraction}>-</button>
                <button className={styles.button} onClick={handleMultiplication}>*</button>
                <button className={styles.button} onClick={handleDivision}>/</button>
            </div>
        </div>
    );
}


`

export {mediaplayerCode, responsiveDesign, creativeDesign, versatileDesign, coopAi} ;
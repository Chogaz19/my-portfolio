import React, { useEffect, useState, useRef } from 'react';
import MonacoEditor from 'react-monaco-editor';

export default function Code({ data }) {
    const options = {
        selectOnLineNumbers: true,
        scrollbar: {
            vertical: 'visible',
            horizontal: 'visible',
            horizontalSliderSize: 5,
            verticalScrollbarSize: 5,
        },
        minimap: {
            enabled: false // Para deshabilitar el minimapa también
        }
    };

    const editorRef = useRef(null);
    const [isMounted, setIsMounted] = useState(false);  // Nuevo estado para rastrear si el componente se ha montado

    const editorDidMount = (editor, monaco) => {
        editor.focus();
        editorRef.current = editor;

        // Definir el tema oscuro
        monaco.editor.defineTheme('darkTheme', {
            base: 'vs-dark',
            inherit: true,
            colors: {
                'editor.foreground': '#d9d9d9',
                'editor.background': '#333333',
                'editorCursor.foreground': '#FFFFFF',
                'editor.lineHighlightBackground': '#333333',
                'editorLineNumber.foreground': '#858585',
                'editor.selectionBackground': '#264F78',
                'editor.inactiveSelectionBackground': '#3A3D41',
                'editorIndentGuide.background': '#404040',
                'editorIndentGuide.activeBackground': '#707070'
            },
            rules: [
                { token: 'comment', foreground: '6A9955' },
                { token: 'variable', foreground: '9CDCFE' },
                { token: 'keyword', foreground: 'C586C0' },
                { token: 'number', foreground: 'B5CEA8' },
                { token: 'string', foreground: 'CE9178' },
                { token: 'type', foreground: '4EC9B0' },
                { token: 'function', foreground: 'DCDCAA' },
                // Añade más reglas según tus necesidades
            ]
        });

        editor.updateOptions({ theme: 'darkTheme' }); // Utilizar el tema oscuro personalizado
    };

    const onChange = (newValue, e) => {
        console.log('onChange', newValue, e);
    };

    useEffect(() => {
        const handleResize = () => {
            if (editorRef.current) {
                editorRef.current.layout();
            }
        };
        window.addEventListener('resize', handleResize);

        return () => window.removeEventListener('resize', handleResize);
    }, []);

    useEffect(() => {
        if (editorRef.current) {
            const model = editorRef.current.getModel();
            model.setValue(data);
        }
    }, [data]);

    useEffect(() => {
        setIsMounted(true); // Marcar el componente como montado después de que se haya montado
    }, []);

    return (
        // Sólo renderizar MonacoEditor después de que el componente se haya montado
        isMounted && (
            <MonacoEditor 
                width="100%"
                height="90%"
                language="javascript"
                theme="darkTheme"
                value={data}
                options={options}
                onChange={onChange}
                editorDidMount={editorDidMount}
            /> 
        )  
    );
}
